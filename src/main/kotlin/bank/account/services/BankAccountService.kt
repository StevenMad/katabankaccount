package bank.account.services

import bank.account.entities.AccountHistoryEntity
import bank.account.entities.ClientEntity
import bank.account.repositories.AccountHistoryRepository
import bank.account.repositories.BankAccountRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*

@Service
class BankAccountService(
    val bankAccountRepository: BankAccountRepository, val accountHistoryRepository: AccountHistoryRepository
) {


    fun depositMoney(client: ClientEntity, money: BigDecimal, date: Date) {
        retrieveAccountAndUpdate(client, money, OperationType.DEPOSIT_TYPE)
        saveHistory(client, money, OperationType.DEPOSIT_TYPE, date)
    }

    fun withdrawMoney(client: ClientEntity, money: BigDecimal, date: Date) {
        retrieveAccountAndUpdate(client, money, OperationType.WITHDRAW_TYPE)
        saveHistory(client, money, OperationType.WITHDRAW_TYPE, date)
    }

    private fun retrieveAccountAndUpdate(
        client: ClientEntity, money: BigDecimal, operationType: OperationType
    ) {
        val account = bankAccountRepository.findByClientName(client.name)
        account.money = operationType.applyOperation(account.money, money)
        bankAccountRepository.save(account)
    }

    private fun saveHistory(
        client: ClientEntity, money: BigDecimal, operationType: OperationType, date: Date
    ) {
        val account = bankAccountRepository.findByClientName(client.name)
        accountHistoryRepository.save(
            AccountHistoryEntity(
                operation = operationType.label,
                amount = money,
                date = date,
                balance = account.money,
                bankAccountEntity = account
            )
        )
    }

    fun getClientAccountHistory(client: ClientEntity): List<AccountHistoryEntity> {
        return accountHistoryRepository.findByBankAccountEntityClientName(client.name)
    }

    enum class OperationType(
        val label: String, val applyOperation: (BigDecimal, BigDecimal) -> BigDecimal
    ) {
        DEPOSIT_TYPE(
            "deposit",
            applyOperation = { accountMoney, depositMoney -> accountMoney + depositMoney }),
        WITHDRAW_TYPE("withdrawal", applyOperation = { accountMoney, withdrawMoney -> accountMoney - withdrawMoney })
    }


}