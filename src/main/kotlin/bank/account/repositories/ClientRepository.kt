package bank.account.repositories

import bank.account.entities.ClientEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ClientRepository : JpaRepository<ClientEntity, Long> {
    fun findByName(name: String): ClientEntity
}