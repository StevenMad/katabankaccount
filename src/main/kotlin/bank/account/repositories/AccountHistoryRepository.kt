package bank.account.repositories

import bank.account.entities.AccountHistoryEntity
import org.springframework.data.jpa.repository.JpaRepository

interface AccountHistoryRepository : JpaRepository<AccountHistoryEntity, Long> {

    fun findByBankAccountEntityClientName(name: String): List<AccountHistoryEntity>

}