package bank.account.repositories

import bank.account.entities.BankAccountEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BankAccountRepository : JpaRepository<BankAccountEntity, Long> {
    fun findByClientName(name: String): BankAccountEntity
}