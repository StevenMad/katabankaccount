package bank.account.utils

import java.time.LocalDate
import java.time.ZoneId
import java.util.*

fun String.toDate(): Date {
    val localDate = LocalDate.parse(this)
    val zoneId = ZoneId.systemDefault()
    val instant = localDate.atStartOfDay(zoneId).toInstant()
    return Date.from(instant)
}