package bank.account.entities

import bank.account.constants.EMPTY_STRING
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
data class AccountHistoryEntity(

    @Id @GeneratedValue(strategy = GenerationType.AUTO) var id: Long? = null,

    @Column(name = "operation") val operation: String = EMPTY_STRING,

    @Column(name = "date") val date: Date,

    @Column(name = "amount") val amount: BigDecimal = BigDecimal.ZERO,

    @Column(name = "balance") val balance: BigDecimal = BigDecimal.ZERO,

    @ManyToOne
    val bankAccountEntity: BankAccountEntity
)
