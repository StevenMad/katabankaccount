package bank.account.entities

import bank.account.constants.EMPTY_STRING
import java.math.BigDecimal
import javax.persistence.*

@Entity
data class BankAccountEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null,

    @Column(name = "account_number")
    val accountNumber: String = EMPTY_STRING,

    @Column(name = "money")
    var money: BigDecimal = BigDecimal.ZERO,

    @OneToOne
    val client: ClientEntity

)