package bank.account.entities

import bank.account.constants.EMPTY_STRING
import javax.persistence.*

@Entity
data class ClientEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long?=null,

    @Column(name = "name") val name: String = EMPTY_STRING

)