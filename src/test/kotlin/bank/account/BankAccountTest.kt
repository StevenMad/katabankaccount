package bank.account

import bank.account.entities.BankAccountEntity
import bank.account.entities.ClientEntity
import bank.account.repositories.AccountHistoryRepository
import bank.account.repositories.BankAccountRepository
import bank.account.repositories.ClientRepository
import bank.account.services.BankAccountService
import bank.account.utils.toDate
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal

@RunWith(SpringRunner::class)
@SpringBootTest
class BankAccountTest {

    @Autowired
    lateinit var bankAccountRepository: BankAccountRepository

    @Autowired
    lateinit var clientRepository: ClientRepository

    @Autowired
    lateinit var bankAccountService: BankAccountService

    @Autowired
    lateinit var accountHistoryRepository: AccountHistoryRepository

    lateinit var client: ClientEntity

    @Before
    fun setUp() {
        //clear
        accountHistoryRepository.deleteAll()
        bankAccountRepository.deleteAll()
        clientRepository.deleteAll()
        //as a client
        clientRepository.save(ClientEntity(name = "Name"))
        client = clientRepository.findByName("Name")
        //having an account
        bankAccountRepository.save(BankAccountEntity(accountNumber = "123", client = client))
    }


    //user story 1
    @Test
    fun `as a client i want to deposit money into my account`() {
        bankAccountService.depositMoney(client, BigDecimal(100), "2022-04-01".toDate())
        val bankAccount = bankAccountRepository.findAll().first()
        Assertions.assertThat(bankAccount.money).isEqualTo(BigDecimal(100).setScale(2))
    }

    //user story 2
    @Test
    fun `as a client in order to use my savings i can withdraw part of my savings`() {
        //Given i have money in my account
        bankAccountService.depositMoney(client, BigDecimal(100), "2022-04-01".toDate())
        //When
        bankAccountService.withdrawMoney(client, BigDecimal(50), "2022-03-01".toDate())
        //Then
        val bankAccount = bankAccountRepository.findByClientName(client.name)
        Assertions.assertThat(bankAccount.money).isEqualTo(BigDecimal(50).setScale(2))
    }

    @Test
    fun `as a client i can withdraw all my savings`() {
        //Given i have money in my account
        bankAccountService.depositMoney(client, BigDecimal(100), "2022-04-01".toDate())
        //When
        bankAccountService.withdrawMoney(client, BigDecimal(100), "2022-03-01".toDate())
        //Then
        val bankAccount = bankAccountRepository.findByClientName(client.name)
        Assertions.assertThat(bankAccount.money).isEqualTo(BigDecimal.ZERO.setScale(2))
    }

    //user story 3
    @Test
    fun `as a client i want to look at my account history`() {
        //Given the following operations
        bankAccountService.depositMoney(client, BigDecimal(100), "2022-01-01".toDate())
        bankAccountService.depositMoney(client, BigDecimal(150), "2022-02-01".toDate())
        bankAccountService.withdrawMoney(client, BigDecimal(100), "2022-03-01".toDate())
        bankAccountService.withdrawMoney(client, BigDecimal(50), "2022-04-01".toDate())
        //When
        val clientHistory = bankAccountService.getClientAccountHistory(client)
        Assertions.assertThat(clientHistory.size).isEqualTo(4)
        Assertions.assertThat(clientHistory.first().operation).isEqualTo("deposit")
        Assertions.assertThat(clientHistory.last().operation).isEqualTo("withdrawal")
        Assertions.assertThat(clientHistory.last().amount).isEqualTo(BigDecimal(50).setScale(2))
        Assertions.assertThat(clientHistory.last().balance).isEqualTo(BigDecimal(100).setScale(2))
    }
}